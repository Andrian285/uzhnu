const jwt = require('jsonwebtoken');
const config = require('../config');
const userManager = require('../db/userManager');

function checkAuth(req, res, next) {
    function unauthorized() {
        res.status(401).json({message: 'Unauthorized'});
    }

    //console.log(req.headers.token);
    if (!req.headers.token) return unauthorized();

    // decode the token using a secret key-phrase
    return jwt.verify(req.headers.token, config.jwtSecret, (err, decoded) => {
        if (err) { return unauthorized(); }

        const userId = decoded.sub;

        userManager.getById(userId)
            .then(user => {
                if (!user) return unauthorized();
                res.locals.user = user;
                next();
            })
            .catch((e) => { return res.status(500).json({message: 'Internal Server Error'}), console.log(e) })
    });
}

module.exports = checkAuth;