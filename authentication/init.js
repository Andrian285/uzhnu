const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const userManager = require('../db/userManager');
const config = require('../config');
const jwt = require('jsonwebtoken');

const serverSalt = require('../config').serverSalt;

const sha512 = require('./hash');

function initPassport() {
    passport.use(new LocalStrategy({
        usernameField: 'email',
        session: false
    },
        function (email, password, done) {
            let hash = sha512(password, serverSalt).passwordHash;
            userManager.getUserByEmailAndPasshash(email, hash)
                .then(user => {
                    const payload = {
                        sub: user._id
                    };
                    const token = jwt.sign(payload, config.jwtSecret, {
                        expiresIn: 60 * 60 * 24 * 30
                    });
                    
                    done(user ? null : 'no user', token, user);
                })
                .catch(err => done('no user', null, null));
        }
    ));
}

module.exports = initPassport;