export default async function fetchData(body, link, method, token, page) {
    try {
        let headers = {'Content-Type': 'application/json', 'Accept': 'application/json'};
        if (token) headers.token = token;
        if (page) headers.page = page;

        let obj = {
            method: method.toLowerCase(),
            headers: headers,
        };

        if (method.toLowerCase() === 'post') obj.body = JSON.stringify(body);

        let response = await fetch(link, obj);
        return response;
    } catch (err) {
        console.log(err);
        return null;
    }

}