export const sendSolution = file => {
    return {type: 'SEND_SOLUTION', file, sendSolution: true};
}

export const resetSolution = () => {
    return {type: 'RESET_SOLUTION', file: null, sendSolution: false};
}

export const setCurrentTask = currentTask => {
    return {type: 'SET_CURRENT_TASK', currentTask};
}

export const setUpdateTaskList = updateTaskList => {
    return {type: 'SET_UPDATE_TASK_LIST', updateTaskList};
}

export const setTaskLoaded = taskLoaded => {
    return {type: 'SET_TASK_LOADED', taskLoaded};
}

export const setTasks = tasks => {
    return {type: 'SET_TASKS', tasks};
}