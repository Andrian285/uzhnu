export const setAuthenticated = authenticated => {
    return {type: 'SET_AUTHENTICATED', authenticated};
}

export const setAuthDataLoaded = authDataLoaded => {
    return {type: 'SET_AUTH_DATA_LOADED', authDataLoaded};
}

export const setUsername = username => {
    return {type: 'SET_USERNAME', username};
}

export const setIsAdmin = isAdmin => {
    return {type: 'SET_IS_ADMIN', isAdmin};
}