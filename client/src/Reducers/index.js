export default function taskReducer(state = [], action) {
    switch (action.type) {
        case 'SEND_SOLUTION':
        case 'RESET_SOLUTION':
            return { ...state, file: action.file, sendSolution: action.sendSolution };
        case 'SET_AUTHENTICATED':
            return { ...state, authenticated: action.authenticated };
        case 'SET_AUTH_DATA_LOADED':
            return { ...state, authDataLoaded: action.authDataLoaded };
        case 'SET_USERNAME':
            return { ...state, username: action.username };
        case 'SET_IS_ADMIN':
            return { ...state, isAdmin: action.isAdmin };
        case 'SET_CURRENT_TASK':
            return {...state, currentTask: action.currentTask};
        case 'SET_TASKS':
            return {...state, tasks: action.tasks};
        case 'SET_UPDATE_TASK_LIST':
            return {...state, updateTaskList: action.updateTaskList};
        case 'SET_TASK_LOADED':
            return {...state, taskLoaded: action.taskLoaded};
        default:
            return state;
    }
}