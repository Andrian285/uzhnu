import React from 'react';
import { Transition, Label, Button, Form, Message } from 'semantic-ui-react';
import 'semantic-ui-css/semantic.min.css';
import '../main.css';
import { matchPath, withRouter } from 'react-router-dom';
import { sendSolution } from '../Actions/TaskActions';
import { setCurrentTask, setTaskLoaded } from '../Actions/TaskActions';
import { connect } from 'react-redux';
import fetchData from '../Auth/Fetch';
import LocalStorageManager from '../Auth/LocalStorageManager';

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            visibleFileFormatError: false,
            fileFormatError: 'Invalid format (should be .cpp) or size (<=10MB)',
            sendDisabled: true,
            file: null,
            fileInfo: ''
        };
        this.loadFile = this.loadFile.bind(this);
    }

    async componentDidMount() {
        this.props.setTaskLoaded(false);
        let taskId = matchPath(this.props.location.pathname, { path:'/tasklist/:id'}).params.id;
        let response = await fetchData(null, '/api/tasks/' + taskId, 'get', LocalStorageManager.getToken());
        let responseData = await response.json();
        if (response.status === 404) this.props.history.push('/404');
        else {
            this.props.setCurrentTask(responseData.task);
            this.props.setTaskLoaded(true);
        }
    }

    async loadFile(evt) {
        const reader = new FileReader();
        let file = evt.target.files[0];
        reader.onloadend = () => {
            let format = file.name.slice(file.name.indexOf('.'));
            if (file.size > 10000000 || format !== '.cpp') {
                this.setState({ visibleFileFormatError: true, sendDisabled: true, fileInfo: '' });
                return;
            }
            else {
                this.setState({ visibleFileFormatError: false, sendDisabled: false, file: reader.result, fileInfo: file.name });
            }
        };
        try {
            reader.readAsBinaryString(file);
        } catch (e) {
            this.setState({ sendDisabled: true, fileInfo: '' });
            //loading canceled
        }
    }

    async removeTask() {
        await fetchData({
            id: this.props.currentTask._id
        }, '/api/tasks/remove', 'post', LocalStorageManager.getToken());
        this.props.history.push('/tasklist');
    }

    render() {
        return (
            <Form>
                <Form.Field>
                    <Message info compact>
                        <Message.Header>Code file extension: .cpp <br />
                            Input file name: input.txt <br />
                            Output file name: output.txt</Message.Header>
                    </Message><br />
                </Form.Field>
                <Form.Field>
                    <Button as='div' className="file-input-wrapper" labelPosition='right'>
                        <Button color='grey' className="btn-file-input">Upload File</Button>
                        <Label basic color='grey' size='medium' content={this.state.fileInfo} className='fileUploadInfo' />
                        <input type="file" name="file" onChange={this.loadFile} />
                    </Button>
                    <br />
                    <Transition visible={this.state.visibleFileFormatError} animation='scale'
                        duration={500}>
                        <Label basic color='red' pointing>{this.state.fileFormatError}</Label>
                    </Transition>
                </Form.Field>
                <Form.Field>
                    {(this.props.authDataLoaded && this.props.isAdmin) &&
                        <Button negative content='remove' onClick={() => this.removeTask()} />}
                    <Button positive content='upload' disabled={this.state.sendDisabled} onClick={() => this.props.sendSolution(this.state.file)} />
                </Form.Field>
            </Form>
        );
    }
}

const mapStateToProps = (state) => ({
    file: state.file,
    authDataLoaded: state.authDataLoaded,
    isAdmin: state.isAdmin,
    currentTask: state.currentTask
});

const mapDispatchToProps = {
    sendSolution,
    setCurrentTask,
    setTaskLoaded
};
const FileUploader = connect(mapStateToProps, mapDispatchToProps)(App);

export default withRouter(FileUploader);