import React from 'react';
import { Dimmer, Loader, Grid, Label, Form, Divider } from 'semantic-ui-react';
import 'semantic-ui-css/semantic.min.css';
import '../main.css';
import fetchData from '../Auth/Fetch';
import LocalStorageManager from '../Auth/LocalStorageManager';

class ProfileData extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: null,
            name: null,
            email: null,
            loading: true
        }
    }

    async componentDidMount() {
        let response = await fetchData(null, '/api/profile', 'get', LocalStorageManager.getToken());
        let responseData = await response.json();
        if (response.status === 200) {
            this.setState({ username: responseData.user.username, name: responseData.user.name, email: responseData.user.email, loading: false });
        }
    }

    render() {
        return (
            <div>
                <Grid columns='2'>
                    <Grid.Column textAlign='right'>
                        <Form>
                            <Form.Field><Label size='large'>Full Name:</Label></Form.Field>
                            <Divider />
                            <Form.Field><Label size='large'>Username:</Label></Form.Field>
                            <Divider />
                            <Form.Field><Label size='large'>E-mail:</Label></Form.Field>
                        </Form>
                    </Grid.Column>
                    <Grid.Column textAlign='left'>
                        <Form>
                            <Form.Field><Label size='large' tag>{this.state.name}</Label></Form.Field>
                            <Divider />
                            <Form.Field><Label size='large' tag>{this.state.username}</Label></Form.Field>
                            <Divider />
                            <Form.Field><Label size='large' tag>{this.state.email}</Label></Form.Field>
                        </Form>
                    </Grid.Column>
                </Grid>
                <Dimmer active={this.state.loading} inverted>
                    <Loader active />
                </Dimmer>
            </div>
        );
    }
}

export default ProfileData;