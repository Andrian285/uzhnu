import React from 'react';
import { List, Dimmer, Loader } from 'semantic-ui-react';
import 'semantic-ui-css/semantic.min.css';
import '../main.css';
import { withRouter } from 'react-router-dom';
import fetchData from '../Auth/Fetch';
import LocalStorageManager from '../Auth/LocalStorageManager';
import { connect } from 'react-redux';
import { setUpdateTaskList, setTasks } from '../Actions/TaskActions';

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false
        }
    }

    componentDidMount() {
        this.updateList();
    }

    async updateList() {
        this.setState({ loading: true });
        this.props.setUpdateTaskList(false);
        let response = await fetchData(null, '/api/tasks', 'get', LocalStorageManager.getToken());
        let responseData = await response.json();
        if (response.status !== 200) console.log(responseData.message);
        else this.props.setTasks(responseData.tasks);
        this.setState({ loading: false });
    }

    renderTasks() {
        if (!this.props.tasks) return;
        let tasks = [];
        for (let task of this.props.tasks) tasks.push(
            <List.Item onClick={() => this.props.history.push('/tasklist/' + task._id)}
                key={task._id}><List.Content><List.Header>{task.name}</List.Header></List.Content></List.Item>
        );
        return tasks;
    }

    render() {
        if (this.props.updateTaskList) this.updateList();
        return (
            <div>
                <Dimmer active={this.state.loading} className='dimmerTaskList' inverted>
                    <Loader size='tiny' />
                </Dimmer>
                <List divided relaxed selection size='large'>
                    {this.renderTasks()}
                </List>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    updateTaskList: state.updateTaskList,
    tasks: state.tasks
});

const mapDispatchToProps = {
    setUpdateTaskList,
    setTasks
};

const TaskListTable = connect(mapStateToProps, mapDispatchToProps)(App);

export default withRouter(TaskListTable);