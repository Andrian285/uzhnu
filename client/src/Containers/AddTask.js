import React from 'react';
import { Button, Modal, Form } from 'semantic-ui-react';
import 'semantic-ui-css/semantic.min.css';
import '../main.css';
import { connect } from 'react-redux';
import { setUpdateTaskList } from '../Actions/TaskActions';
import Alert from 'react-s-alert';
import 'react-s-alert/dist/s-alert-default.css';
import 'react-s-alert/dist/s-alert-css-effects/slide.css';
import fetchData from '../Auth/Fetch';
import LocalStorageManager from '../Auth/LocalStorageManager';

const alertProps = {
    position: 'bottom-left',
    effect: 'slide',
    timeout: 4000
};

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            open: false,
            testsAmount: 1,
            name: null,
            task: null,
            tests: [{
                input: null,
                output: null
            }],
            sendDisabled: true,
            sendLoading: false
        };
        this.inputLoaded = this.inputLoaded.bind(this);
        this.outputLoaded = this.outputLoaded.bind(this);
        this.taskLoaded = this.taskLoaded.bind(this);
    }

    show = () => this.setState({ open: true });
    close = () => this.setState({ open: false });
    handleNameChange = e => this.setState({ name: e.target.value }, () => this.checkFieldsCorrectness());
    increase = () => {
        let tests = this.state.tests.slice();
        tests.push({
            input: null,
            output: null
        });
        this.setState({ testsAmount: this.state.testsAmount + 1, tests }, () => this.checkFieldsCorrectness());
    }

    renderTests() {
        let tests = [];
        for (let i = 1; i <= this.state.testsAmount; i++) tests.push(
            <Form.Group key={i}>
                <Form.Input label={'Input' + i} type='file' onChange={(e) => this.readFile(e, this.inputLoaded, i - 1)} />
                <Form.Input label={'Output' + i} type='file' onChange={(e) => this.readFile(e, this.outputLoaded, i - 1)} />
            </Form.Group>
        );
        return tests;
    }

    readFile(evt, cb, index) {
        const reader = new FileReader();
        let file = evt.target.files[0];
        reader.onloadend = () => {
            if (file.size > 10000000) {
                return Alert.error('Incorrect file size (should be <=10MB)', alertProps);
            }
            else cb(reader.result, index);
        };
        try {
            reader.readAsText(file);
        } catch (e) {
            //loading canceled
        }
    }

    checkFieldsCorrectness() {
        if (!this.state.name || !this.state.task) return this.setState({ sendDisabled: true });
        for (let test of this.state.tests) if (!test.input || !test.output) return this.setState({ sendDisabled: true });
        this.setState({ sendDisabled: false });
    }

    inputLoaded(file, index) {
        let tests = this.state.tests.slice();
        tests[index].input = file;
        this.setState({ tests }, () => this.checkFieldsCorrectness());
    }

    outputLoaded(file, index) {
        let tests = this.state.tests.slice();
        tests[index].output = file;
        this.setState({ tests }, () => this.checkFieldsCorrectness());
    }

    taskLoaded(file) {
        this.setState({ task: file }, () => this.checkFieldsCorrectness());
    }

    async fetchData() {
        this.setState({sendLoading: true});
        let response = await fetchData({
            name: this.state.name,
            task: this.state.task,
            tests: this.state.tests
        }, '/api/tasks/add', 'post', LocalStorageManager.getToken());
        let responseData = await response.json();
        if (response.status !== 200) Alert.error(responseData.message, alertProps);
        else {
            this.close();
            this.props.setUpdateTaskList(true);
        }
        this.setState({sendLoading: false});
    }

    render() {
        return (
            <div>
                {this.props.isAdmin && <Button content='+' positive onClick={this.show} />}
                <Modal dimmer={false} open={this.state.open} onClose={this.close}>
                    <Modal.Header>Add new task</Modal.Header>
                    <Modal.Content>
                        <Form>
                            <Form.Input fluid label='Task Name' placeholder='Task Name' onChange={this.handleNameChange} />
                            <Form.Input fluid label='The task' type='file' onChange={(e) => this.readFile(e, this.taskLoaded)} />
                            {this.renderTests()}
                            <Form.Button content='Add test' onClick={this.increase} />
                        </Form>
                    </Modal.Content>
                    <Modal.Actions>
                        <Button color='black' onClick={this.close} content='Cancel' />
                        <Button positive icon='checkmark' labelPosition='right' loading={this.state.sendLoading}
                        content="Submit" onClick={() => this.fetchData()} disabled={this.state.sendDisabled} />
                    </Modal.Actions>
                </Modal>
                <Alert stack={{ limit: 3 }} />
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    isAdmin: state.isAdmin
});

const mapDispatchToProps = {
    setUpdateTaskList
};

const AddProblem = connect(mapStateToProps, mapDispatchToProps)(App);

export default AddProblem;