import React from 'react';
import validator from 'email-validator';
import { Form, Input, Icon, Button, Label, Transition } from 'semantic-ui-react';
import Warning from '../../Components/Common/Warning';
import LocalStorageManager from '../../Auth/LocalStorageManager';
import fetchData from '../../Auth/Fetch';
import { setAuthenticated } from '../../Actions/AuthActions';
import { connect } from 'react-redux';

const fields = [
    {
        value: 'Full name',
        id: 'name',
        err: 'Incorrect Full Name: name and surname should be from capital letters',
        requirement: name => { return /^[A-Z]+[a-z]+\s+[A-Z]+[a-z]+$/.test(name); }
    },
    {
        value: 'Username',
        id: 'username',
        err: 'Incorrect Username: must be at least 3 characters',
        requirement: username => { return username.length >= 3; }
    },
    {
        value: 'Email',
        id: 'email',
        err: 'Incorrect e-mail',
        requirement: email => { return validator.validate(email); }
    },
    {
        value: 'Password',
        id: 'password',
        err: 'Incorrect password: must be at least 8 characters',
        requirement: pass => { return pass.length >= 8; }
    },
    {
        value: 'Confirm',
        id: 'confirm',
        err: 'The password and its confirm are not the same',
        requirement: null
    },
];

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            warning: null,
            loading: false,
            fields: [],
            visibleErrors: []
        };
        this.handleChange = this.handleChange.bind(this);
    }

    addField(id, tmpFields, tmpVisibleErrors) {
        tmpFields.push({ ...fields.find(x => x.id === id), data: '' });
        tmpVisibleErrors.push({ id, statement: false });
    }

    componentDidMount() {
        let tmpFields = [], tmpVisibleErrors = [];
        if (this.props.name) this.addField('name', tmpFields, tmpVisibleErrors);
        if (this.props.username) this.addField('username', tmpFields, tmpVisibleErrors);
        if (this.props.email) this.addField('email', tmpFields, tmpVisibleErrors);
        if (this.props.password) this.addField('password', tmpFields, tmpVisibleErrors);
        if (this.props.confirm) this.addField('confirm', tmpFields, tmpVisibleErrors);
        this.setState({ fields: tmpFields, visibleErrors: tmpVisibleErrors });
    }

    async processReceivedData(data) {
        this.setState({ loading: false });
        let responseData = await data.json();
        let status = [500, 400, 401, 403];
        if (!status.includes(data.status)) {
            if (responseData.token) {
                LocalStorageManager.authenticateUser(responseData.token, responseData.isAdmin);
                this.props.setAuthenticated(true);
            }
        }
        else {
            this.setState({ warning: responseData.message });
            this.setState({ formValid: false });
        }
    }

    async fetchData() {
        let data = {};
        for (let field of this.state.fields) data[field.id] = field.data;

        let responseData = await fetchData(data, this.props.fetchTo, 'post');
        this.processReceivedData(responseData);
    }

    getErrorIndex(id) {
        return this.state.visibleErrors.indexOf(this.state.visibleErrors.find(x => x.id === id));
    }

    getFieldIndex(id) {
        return this.state.fields.indexOf(this.state.fields.find(x => x.id === id));
    }

    setFormValid() {
        this.setState({ loading: true });
        let statements = [];
        for (let err of this.state.visibleErrors) {
            let index = this.state.visibleErrors.indexOf(err);
            let field = this.state.fields[this.getFieldIndex(err.id)];
            if (err.id !== 'confirm') statements.push(this.toggleError(index, field.requirement(field.data)));
            else statements.push(this.toggleError(index, this.state.fields[this.getFieldIndex('password')].data === field.data));
        }
        if (statements.find(x => x === false) === undefined) this.fetchData();
        else this.setState({ loading: false });
    }

    toggleError(errId, requirement) {
        if (requirement === this.state.visibleErrors[errId].statement) {
            let err = this.state.visibleErrors;
            err[errId].statement = !err[errId].statement;
            this.setState({ visibleErrors: err });
        }
        return requirement;
    }

    handleChange(e) {
        const field = e.target.name, errId = this.getErrorIndex(field), fieldId = this.getFieldIndex(field), fieldData = e.target.value;
        let tmpFields = this.state.fields;
        tmpFields[fieldId].data = fieldData;
        this.setState({ fields: tmpFields }, () => {
            if (field !== 'confirm') this.toggleError(errId, this.state.fields[fieldId].requirement(fieldData));
            else this.toggleError(errId, this.state.fields[this.getFieldIndex('password')].data === fieldData);
        });
    }

    renderFields() {
        let formFields = [];
        for (let id in this.state.fields) formFields.push(
            <Form.Field key={id}>
                <label>{this.state.fields[id].value}</label>
                <Input type={this.state.fields[id].id === 'password' || this.state.fields[id].id === 'confirm' ? 'password' : 'text'}
                    placeholder={this.state.fields[id].value} onChange={this.handleChange} name={this.state.fields[id].id} />
                <Transition visible={this.state.visibleErrors[id].statement} animation='scale' duration={500}>
                    <Label basic color='red' pointing>{this.state.fields[id].err}</Label>
                </Transition>
            </Form.Field>
        )
        return formFields;
    }

    render() {
        return (
            <div>
                <Warning warning={this.state.warning} />
                <Form>
                    {this.renderFields()}

                    <Form.Field>
                        <Button positive animated onClick={() => this.setFormValid()} loading={this.state.loading}>
                            <Button.Content visible>Submit</Button.Content>
                            <Button.Content hidden>
                                <Icon name='right arrow' />
                            </Button.Content>
                        </Button>
                    </Form.Field>
                </Form>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    authenticated: state.authenticated
});

const mapDispatchToProps = {
    setAuthenticated
};
const AuthForm = connect(mapStateToProps, mapDispatchToProps)(App);

export default AuthForm;
