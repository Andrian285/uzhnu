import React from 'react';
import { Menu, Container, Dropdown, Grid } from 'semantic-ui-react';
import 'semantic-ui-css/semantic.min.css';
import '../../main.css';
import { withRouter } from 'react-router-dom';
import LocalStorageManager from '../../Auth/LocalStorageManager';
import { setAuthenticated, setAuthDataLoaded, setUsername, setIsAdmin } from '../../Actions/AuthActions';
import { connect } from 'react-redux';
import fetchData from '../../Auth/Fetch';

class App extends React.Component {
    async componentDidMount() {
        if (!LocalStorageManager.getToken()) {
            this.props.setAuthDataLoaded(true);
            this.props.setAuthenticated(false);
            return;
        }
        let response = await fetchData(null, '/api/auth/checkauth', 'get', LocalStorageManager.getToken());
        let responseData = await response.json();
        if (response.status !== 200) {
            this.props.setAuthenticated(false);
            LocalStorageManager.deauthenticateUser();
        } else {
            this.props.setAuthenticated(true);
            if (this.props.username !== responseData.username) this.props.setUsername(responseData.username);
            if (this.props.isAdmin !== responseData.isAdmin) this.props.setIsAdmin(responseData.isAdmin);
        }
        this.props.setAuthDataLoaded(true);
    }

    rightMenu() {
        const { location, history } = this.props;
        if (!this.props.authenticated && this.props.authDataLoaded) return (
            <Grid>
                <Grid.Row>
                    <Menu.Item name='sign in' active={location.pathname === '/signin'} onClick={() => history.push('/signin')} />
                    <Menu.Item name='sign up' active={location.pathname === '/signup'} onClick={() => history.push('/signup')} />
                </Grid.Row>
            </Grid>
        );
        return (
            <Dropdown item text={this.props.username} size='huge' pointing loading={!this.props.authDataLoaded}>
                <Dropdown.Menu className='dropdownMenu'>
                    <Dropdown.Item onClick={() => history.push('/profile')}>Profile</Dropdown.Item>
                    <Dropdown.Item onClick={() => {
                        LocalStorageManager.deauthenticateUser();
                        this.props.setAuthenticated(false);
                    }
                    }>Logout</Dropdown.Item>
                </Dropdown.Menu>
            </Dropdown>
        );
    }

    render() {
        const { history, location, authenticated } = this.props;
        return (
            <div className='globalMenu'>
                <Menu size='huge' pointing secondary>
                    <Container>
                        <Menu.Item name='home' active={location.pathname === '/'} onClick={() => history.push('/')} />
                        {authenticated && <Menu.Item name='task list' active={location.pathname === '/tasklist'} onClick={() => history.push('/tasklist')} />}
                        <Menu.Menu position='right'>
                            {this.rightMenu()}
                        </Menu.Menu>
                    </Container>
                </Menu>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    authenticated: state.authenticated,
    authDataLoaded: state.authDataLoaded,
    username: state.username,
    isAdmin: state.isAdmin
});

const mapDispatchToProps = {
    setAuthenticated,
    setAuthDataLoaded,
    setUsername,
    setIsAdmin
};
const GlobalMenu = connect(mapStateToProps, mapDispatchToProps)(App);

export default withRouter(GlobalMenu);
