import React from 'react';
import { Segment, Header, TextArea, Form } from 'semantic-ui-react';
import 'semantic-ui-css/semantic.min.css';
import '../main.css';
import { resetSolution, setCurrentTask } from '../Actions/TaskActions';
import { connect } from 'react-redux';
import fetchData from '../Auth/Fetch';
import LocalStorageManager from '../Auth/LocalStorageManager';

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            results: '',
            score: 0,
            resultHeader: 'Result:',
            resultsLoading: false
        };
    }

    async sendData() {
        this.props.resetSolution();
        this.setState({ resultsLoading: true });

        let response = await fetchData({
            file: this.props.file,
            task: this.props.currentTask
        }, '/api/tasks/task/uploadFile', 'post', LocalStorageManager.getToken());
        let responseData = await response.json();
        let status = [500, 400, 401, 403];
        if (!status.includes(response.status)) {
            this.setState({ results: responseData.results, resultHeader: 'Result: ' + responseData.score });
            this.props.setCurrentTask(responseData.task);
            console.log(responseData.task);
        }
        else this.setState({ results: responseData.message, resultHeader: 'Result:' });
        this.setState({ resultsLoading: false });
    }

    componentDidUpdate() {
        if (this.props.sendSolution) this.sendData();
    }

    render() {
        return (
            <Segment basic loading={this.state.resultsLoading}>
                <Form>
                    <Header as='h3' content={this.state.resultHeader} />
                    <TextArea autoHeight value={this.state.results} />
                </Form>
            </Segment>
        );
    }
}

const mapStateToProps = (state) => ({
    file: state.file,
    sendSolution: state.sendSolution,
    currentTask: state.currentTask
});

const mapDispatchToProps = {
    resetSolution,
    setCurrentTask
};
const Results = connect(mapStateToProps, mapDispatchToProps)(App);

export default Results;