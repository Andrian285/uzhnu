import React from 'react';
import { Container, Segment, Header, Icon, Grid, Message, Dimmer, Loader } from 'semantic-ui-react';
import { Redirect } from 'react-router-dom';
import 'semantic-ui-css/semantic.min.css';
import '../main.css';
import FileUploader from '../Containers/FileUploader';
import Results from '../Containers/Results';
import GlobalMenu from '../Containers/Common/GlobalMenu';
import LocalStorageManager from '../Auth/LocalStorageManager';
import UserList from './UserList';
import { connect } from 'react-redux';

const App = (props) => {
    if (!LocalStorageManager.isUserAuthenticated() || (!props.authenticated && props.authDataLoaded)) return <Redirect to='/signup' />;
    return (
        <div>
            <GlobalMenu />
            <Container>
                <br />
                <Segment textAlign='center'>
                    <Header as='h2' icon>
                        <Icon name='code' />
                        <span className='pageHeader'>UzhNU Programming Workspace</span>
                    </Header>
                </Segment>
                <Segment>
                    {props.taskLoaded ?
                        <div>{props.currentTask && <Message className='lineBreak' header={props.currentTask.name} content={props.currentTask.task} />}<br/></div>
                        :
                        <Dimmer active>
                            <Loader content='Loading' />
                        </Dimmer>
                    }
                    <Grid columns={2}>
                        <Grid.Column textAlign='left'>
                            <FileUploader />
                        </Grid.Column>
                        <Grid.Column>
                            <Results />
                        </Grid.Column>
                    </Grid>
                    <br/>
                    <UserList />
                </Segment>
            </Container>
        </div>
    );
}

const mapStateToProps = (state) => ({
    authenticated: state.authenticated,
    authDataLoaded: state.authDataLoaded,
    currentTask: state.currentTask,
    taskLoaded: state.taskLoaded
});

const Task = connect(mapStateToProps)(App);

export default Task;
