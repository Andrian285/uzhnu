import React from 'react';
import { Container, Segment, Header } from 'semantic-ui-react';
import { Redirect } from 'react-router-dom';
import 'semantic-ui-css/semantic.min.css';
import '../main.css';
import GlobalMenu from '../Containers/Common/GlobalMenu';
import AuthForm from '../Containers/Common/AuthForm';
import LocalStorageManager from '../Auth/LocalStorageManager';
import { connect } from 'react-redux';

const App = (props) => {
    if (LocalStorageManager.isUserAuthenticated() || (props.authenticated && props.authDataLoaded)) return <Redirect to='/' />;
    return (
        <div>
            <GlobalMenu />
            <Container text textAlign='justified'>
                <br />
                <Segment>
                    <Header as='h2' textAlign='center'>Sign In</Header>
                </Segment>
                <Segment textAlign='center'>
                    <AuthForm password email fetchTo='/api/auth/signin' />
                </Segment>
            </Container>
        </div>
    );
}

const mapStateToProps = (state) => ({
    authenticated: state.authenticated,
    authDataLoaded: state.authDataLoaded
});

const SignIn = connect(mapStateToProps)(App);

export default SignIn;
