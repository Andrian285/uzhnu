import React from 'react';
import { Container, Segment } from 'semantic-ui-react';
import { Redirect } from 'react-router-dom';
import 'semantic-ui-css/semantic.min.css';
import '../main.css';
import TaskListTable from '../Containers/TaskListTable';
import AddTask from '../Containers/AddTask';
import GlobalMenu from '../Containers/Common/GlobalMenu';
import LocalStorageManager from '../Auth/LocalStorageManager';
import { connect } from 'react-redux';

const App = (props) => {
    if (!LocalStorageManager.isUserAuthenticated() || (!props.authenticated && props.authDataLoaded)) return <Redirect to='/signup' />;
    return (
        <div>
            <GlobalMenu />
            <Container textAlign='center'>
                <br />
                <Segment>
                    <TaskListTable />
                </Segment>
                <AddTask />
            </Container>
        </div>
    );
}

const mapStateToProps = (state) => ({
    authenticated: state.authenticated,
    authDataLoaded: state.authDataLoaded,
    isAdmin: state.isAdmin
});

const TaskList = connect(mapStateToProps)(App);

export default TaskList;
