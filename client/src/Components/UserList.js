import React from 'react';
import { Table } from 'semantic-ui-react';
import 'semantic-ui-css/semantic.min.css';
import '../main.css';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

function renderUsers(props) {
    if (!props.currentTask) return;
    let users = [];
    for (let user of props.currentTask.users) users.push(
        <Table.Row key={user._id}>
            <Table.Cell>{user.username}</Table.Cell>
            <Table.Cell>{user.score}</Table.Cell>
        </Table.Row>
    );
    return users;
}

const App = props => {
    return (
        <div>
            {(props.currentTask && props.currentTask.users.length > 0) &&
                <Table celled selectable>
                    <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell>Userame</Table.HeaderCell>
                            <Table.HeaderCell>Score</Table.HeaderCell>
                        </Table.Row>
                    </Table.Header>

                    <Table.Body>{renderUsers(props)}</Table.Body>
                </Table>}
        </div>
    );
}

const mapStateToProps = (state) => ({
    currentTask: state.currentTask
});

const UserList = connect(mapStateToProps)(App);

export default withRouter(UserList);