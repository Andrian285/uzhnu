import React from 'react';
import { Container, Segment, Header, Image } from 'semantic-ui-react';
import 'semantic-ui-css/semantic.min.css';
import '../main.css';
import GlobalMenu from '../Containers/Common/GlobalMenu';

const NotFound = (props) => {
    return (
        <div>
            <GlobalMenu />
            <Container text textAlign='center'>
                <br />
                <Segment>
                <Header as='h2' textAlign='center' color='red'>404: Not Found</Header>
                    <Image src={require('../images/notfound.gif')} />
                </Segment>
            </Container>
        </div>
    );
}

export default NotFound;
