import React from 'react';
import { Container, Segment, Header, Icon, Divider, Button } from 'semantic-ui-react';
import { NavLink } from 'react-router-dom';
import 'semantic-ui-css/semantic.min.css';
import '../main.css';
import GlobalMenu from '../Containers/Common/GlobalMenu';

const Home = (props) => {
    return (
        <div>
            <GlobalMenu />
            <Container textAlign='center'>
                <br />
                <Segment>
                    <Header as='h2' icon>
                        <Icon name='code' />
                        <span className='pageHeader'>UzhNU Programming Workspace</span>
                    </Header>
                </Segment>
                <Segment textAlign='center'>
                    <Container text textAlign='left'>
                        This website has been developed for students who are obtaining bachelor's degree in Computer Engeneering speciality
                        at Faculty of Engeneering, UzhNU. The main aim is to test the code, written for solving tasks that was prepared for 
                        programmers in order to check their competence. To start using the website, you need to sign up your account. After that you 
                        may login and choose some task from the list. After viewing the task and writing your code, you may 
                        upload it on the website and click "send". If all steps are passed correctly, the website will test 
                        your code and show your final score, which calculates accordingly to the amount of passed tests.
                        <Divider />
                        Даний сайт був розроблений для студентів інженерно-технічного факультету УжНУ спеціальності "Комп'ютерна інженерія". 
                        Основна мета сайту полягає в тому, щоб перевірити коректність коду, написаного для вирішення задач,
                        які були підготовлені для програмістів з метою перевірки їхньої компетентності. Щоб розпочати
                        використання сайту, вам необхідно зареєструвати свій обліковий запис. Після цього ви можете
                        залогінитись та вибрати яку-небудь задачу зі списку. Після читання умови задачі та написання коду,
                        ви можете завантажити його на даний сайт та натиснути "send". Якщо всі дії були виконані
                        правильно, сайт протестує ваш код та виставить кінцеву кількість балів, які обраховуються в
                        залежності від кількості пройдених тестів.
                    </Container>
                    <Divider />
                    <NavLink to='/tasklist'>
                        <Button>Get Started</Button>
                    </NavLink>
                </Segment>
            </Container>
        </div>
    );
}

export default Home;
