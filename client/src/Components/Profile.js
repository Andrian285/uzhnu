import React from 'react';
import { Container, Segment, Header, Icon } from 'semantic-ui-react';
import { Redirect } from 'react-router-dom';
import 'semantic-ui-css/semantic.min.css';
import '../main.css';
import GlobalMenu from '../Containers/Common/GlobalMenu';
import ProfileData from '../Containers/ProfileData';
import LocalStorageManager from '../Auth/LocalStorageManager';
import { connect } from 'react-redux';


const App = (props) => {
    if (!LocalStorageManager.isUserAuthenticated() || (!props.authenticated && props.authDataLoaded)) return <Redirect to='/signup' />;
    return (
        <div>
            <GlobalMenu />
            <Container textAlign='center'>
                <br />
                <Segment inverted attached='top'>
                    <Header as='h2' icon>
                        <Icon name='user' />
                        <span className='pageHeader'>Profile</span>
                    </Header>
                </Segment>
                <Segment attached='bottom' textAlign='center'>
                    <ProfileData />
                </Segment>
            </Container>
        </div>
    );
}

const mapStateToProps = (state) => ({
    authenticated: state.authenticated,
    authDataLoaded: state.authDataLoaded
});

const Profile = connect(mapStateToProps)(App);

export default Profile;
