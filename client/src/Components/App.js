import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import Reducer from '../Reducers';
import Task from './Task';
import SignUp from './SignUp';
import SignIn from './SignIn';
import NotFound from './NotFound';
import Home from './Home';
import Profile from './Profile';
import TaskList from './TaskList';

const store = createStore(Reducer);

export default class App extends React.Component {
    render() {
        return (
            <Provider store={store}>
                <BrowserRouter>
                    <Switch>
                        <Route exact path="/" component={Home} />
                        <Route exact path="/signup" component={SignUp} />
                        <Route exact path="/signin" component={SignIn} />
                        <Route exact path="/profile" component={Profile} />
                        <Route exact path="/tasklist" component={TaskList} />
                        <Route exact path="/tasklist/:id" component={Task} />
                        <Route component={NotFound} />
                    </Switch>
                </BrowserRouter>
            </Provider>
        );
    }
}