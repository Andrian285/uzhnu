const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const busboyBodyParser = require('busboy-body-parser');
const passport = require('passport');
const path = require("path");
require('dotenv').config();

app.use(bodyParser.json({
    limit: '10mb',
    extended: false
}));
app.use(busboyBodyParser({ limit: '15mb' }));
app.use(passport.initialize());

app.use(express.static(path.join(__dirname + '/client/', 'build')));

require('./authentication').init();

require('./routes').init(app);

app.get('/*', function (req, res) {
    res.sendFile(path.join(__dirname + '/client/build/index.html'));
});

const server = app.listen(process.env.PORT, () => {
    console.log('server is listening at %s', process.env.PORT);
});