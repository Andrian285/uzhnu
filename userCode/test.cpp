#include <fstream>

using namespace std;

int main ()
{
	int n;
	ifstream fin ("input.txt");
	ofstream fout ("output.txt");
	fin >> n;
	int sum = 0;
	for (int i=0; i<n; i++) {
		int tmp;
		fin >> tmp;
		sum += tmp;
	}
	fout << sum << endl;
	fin.close ();
	fout.close ();
	return 0;
}
