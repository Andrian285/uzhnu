const mongoose = require('mongoose');

let User = mongoose.model('User', {
	name: String, 
	username: String,
	passwordHash: String,
	email: String,
    role: String
});

module.exports = User;