const mongoose = require('mongoose');

let Task = mongoose.model('Task', {
    name: String, 
    task: String,
    tests: [{
        input: String,
        output: String
    }],
    users: [{
        id: mongoose.Schema.Types.ObjectId,
        username: String,
        score: Number
    }]
});

module.exports = Task;