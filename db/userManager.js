const mongoose = require('mongoose');
const User = require('../models/user');

require('./connection'); 

exports.create = function (user) {
    let entity = new User (user);
    return entity.save();
};

exports.getAll = function () {
    return User.find().exec();
};

exports.getById = function (user_id) {
    return User.findOne({ _id: user_id}).exec();
};

exports.update = function (user_id, user) {
    let entity = new User (user);
    return User.findOneAndUpdate({ _id: user_id}, entity);
};

exports.remove = function (user_id) {
    return User.remove({ _id: user_id});
};

exports.getUserByEmailAndPasshash = function (email, passHash) {
    return User.findOne({ email: email, passwordHash: passHash}).exec();
}

exports.isAlreadyExist = async function(username, email) {
    let uname = await User.findOne({username: username}).exec();
    let mail = await User.findOne({email: email}).exec();
    if (!uname && !mail) return false;
    return true;
}

exports.updatePhoto = function (user_id, photo) {
    //console.log(photo.mimetype);
    return User.update({_id: user_id}, {
        photo: photo
    });
}

exports.getCount = function () {
    return User.count({});
}