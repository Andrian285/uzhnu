const mongoose = require('mongoose');
const Task = require('../models/task');

require('./connection'); 

exports.create = function (task) {
    let entity = new Task (task);
    return entity.save();
};

exports.update = function (task_id, task) {
    let entity = new Task (task);
    return Task.findOneAndUpdate({ _id: task_id}, entity);
};

exports.isUserExists = function (task_id, user_id) {
    return Task.findOne({_id: task_id, 'users.id': user_id}).exec();
}

exports.updateUserScore = function (task_id, user_id, score) {
    return Task.findOneAndUpdate({_id: task_id, 'users.id': user_id}, {'$set': {
        'users.$.score': score
    }}).exec();
}

exports.getAll = function () {
    return Task.find().exec();
};

exports.getById = function (task_id) {
    return Task.findOne({ _id: task_id}).exec();
};

exports.removeById = function (task_id) {
    return Task.remove({ _id: task_id});
}

exports.isAlreadyExist = function(name) {
    return Task.findOne({ name: name}).exec();
}
