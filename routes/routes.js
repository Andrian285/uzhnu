function initRoutes(app) {
    const tasksRouter = require('./tasks');
    app.use("/api/tasks", tasksRouter);

    const authRouter = require('./auth');
    app.use("/api/auth", authRouter);

    const profileRouter = require('./profile');
    app.use("/api/profile", profileRouter);

    const projectsRouter = require('./projects');
    app.use("/api/projects", projectsRouter);

    app.post("/*", (req, res) => {
        res.status(404).json({message: 'Not Found'});
    });
}

module.exports = initRoutes;