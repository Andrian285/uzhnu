const express = require('express');
const router = express.Router();
const fs = require('fs');
const util = require('util');
const exec = util.promisify(require('child_process').exec);
const taskManager = require('../db/taskManager');
const checkAuth = require('../middleware/checkAuth');

const userCodePath = './userCode/';
const userCodeFN = 'test.cpp';
const inputFN = 'input.txt';
const outputFN = 'output.txt';

function writeToFile(path, data) {
    return new Promise((resolve, reject) => {
        fs.writeFile(path, data, function (err) {
            if (err) {
                reject(err);
            } else resolve("Success");
        });
    });
}

function readFile(path) {
    return new Promise((resolve, reject) => {
        fs.readFile(path, 'utf8', function (err, data) {
            if (err) reject(err);
            else resolve(data);
        });
    });
}

router.post('/uploadFile', checkAuth, async (req, res) => {
    try {
        await writeToFile(userCodePath + userCodeFN, req.body.file);
        let tests = req.body.task.tests;
        let scorePerTest = 10 / tests.length;

        let results = '', score = 0;
        for (let id in tests) {
            await writeToFile(userCodePath + inputFN, tests[id].input);
            await exec('cd ' + userCodePath + '\n g++ ' + userCodeFN);
            const { stdout, stderr } = await exec('cd ' + userCodePath + '\n timeout 2s ./a.out');
            let answer = await readFile(userCodePath + outputFN);
            if (answer.trim() === tests[id].output.trim()) score += scorePerTest, results += 'Test ' + id + ': ' + scorePerTest + '\n';
            else results += 'Test ' + id + ': ' + 0 + '\n';
        }
        score = Number((score).toFixed(1));
        let task = req.body.task;
        let exist = await taskManager.updateUserScore(task._id, res.locals.user._id, score);
        if (!exist) {
            let user = {
                id: res.locals.user._id,
                username: res.locals.user.username,
                score: score
            }
            task.users.push(user);
            await taskManager.update(task._id, {
                _id: task._id,
                name: task.name,
                task: task.task,
                tests: tests,
                users: task.users
            });
        }
        else task = await taskManager.getById(task._id);
        res.json({ score, results, task });
    } catch (err) {
        console.log(err);
        return res.status(400).json({ message: 'Invalid data' });
    }
});

module.exports = router;