const express = require('express');
const router = express.Router();
const taskManager = require('../db/taskManager');
const checkAuth = require('../middleware/checkAuth');

router.get('/', checkAuth, async (req, res) => {
    try {
        let tasks = await taskManager.getAll();
        res.json({tasks});
    } catch (err) {
        console.log(err);
        return res.status(500).json({ message: 'Internal Server Error' });
    }
});

router.post('/add', checkAuth, async (req, res) => {
    if (res.locals.user.role !== 'admin') return res.status(403).json({message: 'Not Admin'});
    if (!req.body.name || !req.body.task || !req.body.tests) return res.status(400).json('Missing some data');
    try {
        let task = {
            name: req.body.name,
            task: req.body.task,
            tests: req.body.tests,
            users: []
        }
        await taskManager.create(task);
        return res.json({message: 'success'});
    } catch (err) {
        console.log(err);
        return res.status(500).json({ message: 'Internal Server Error' });
    }
});

router.post('/remove', checkAuth, async (req, res) => {
    if (res.locals.user.role !== 'admin') return res.status(403).json({message: 'Not Admin'});
    try {
        await taskManager.removeById(req.body.id);
        res.json({message: 'success'});
    } catch (err) {
        console.log(err);
        return res.status(500).json({ message: 'Internal Server Error' });
    }
});

const taskRouter = require('./task');
router.use("/task", taskRouter);

router.get('/:id', checkAuth, async (req, res) => {
    try {
        let task = await taskManager.getById(req.params.id);
        res.json({task});
    } catch (err) {
        console.log(err);
        return res.status(500).json({ message: 'Internal Server Error' });
    }
});

module.exports = router;