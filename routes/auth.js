const passport = require('passport');
const userManager = require('../db/userManager');
const express = require('express');
const router = express.Router();
const checkAuth = require('../middleware/checkAuth');
const validation = require('../middleware/validation');
const sha512 = require('../authentication').sha512;
const serverSalt = require('../config').serverSalt; 

router.post('/signup', validation, (req, res) => {
    userManager.getAll()
        .then(datalist => {
            let user = {
                name: req.body.name,
                username: req.body.username,
                passwordHash: sha512(req.body.password, serverSalt).passwordHash,
                email: req.body.email,
                role: 'user'
            };
            userManager.isAlreadyExist(req.body.username, req.body.email)
                .then(statement => {
                    if (statement) res.status(400).json({ message: 'Username or e-mail is already exist' });
                    else {
                        userManager.create(user)
                            .then(() => {
                                res.json({ message: 'success' });
                            });
                    }
                })
                .catch(err => { console.log(err); res.status(500).json({message: 'Internal Server Error'}); });

        })
        .catch(err => { console.log(err); res.status(500).json({message: 'Internal Server Error'}); });
});

router.post('/signin', (req, res, next) => {
    passport.authenticate('local', (err, token, userData) => {
        if (err) return res.status(400).json({
            message: 'Incorrect e-mail or password'
        });
        return res.json({
            token,
            isAdmin: userData.role === 'admin'
        });
    })(req, res, next);
});

router.get('/checkauth', checkAuth, (req, res, next) => {
    return res.json({username: res.locals.user.username, isAdmin: res.locals.user.role === 'admin'});
});

module.exports = router;