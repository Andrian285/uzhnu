const passport = require('passport');
const userManager = require('../db/userManager');
const express = require('express');
const router = express.Router();
const checkAuth = require('../middleware/checkAuth');

router.get('/', checkAuth, (req, res, next) => {
    return res.json({user: res.locals.user});
});

module.exports = router;